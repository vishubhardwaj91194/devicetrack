package com.daffodil.devicetrack.activities.adminloginscreen.model;

public class AdminCredentialModel {
    /**
     * class variables for the model
     */
    public String usedId;
    public String userType;
    public String token;
    public String _id;
    public String email;
    public String fullName;

    /**
     * <p>
     *     this method is used to return userId
     * </p>
     * @return userId
     */
    public String getUsedId() {
        return this.usedId;
    }

    /**
     * <p>
     *     this method is used to set userId
     * </p>
     * @param userId userId passed as String
     */
    public void setUserId(String userId) {
        this.usedId = userId;
    }

    /**
     * <p>
     *     this method is used to return userType
     * </p>
     * @return userType
     */
    public String getUserType() {
        return this.userType;
    }

    /**
     * <p>
     *     this method is used to set userType
     * </p>
     * @param userType userType passed as String
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * <p>
     *     this method is used to return token
     * </p>
     * @return token
     */
    public String getToken() {
        return this.token;
    }

    /**
     * <p>
     *     this method is used to set token
     * </p>
     * @param token token passed as String
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * <p>
     *     this method is used to return _id
     * </p>
     * @return _id
     */
    public String get_id() {
        return this._id;
    }

    /**
     * <p>
     *     this method is used to set _id
     * </p>
     * @param _id _id passed as String
     */
    public void set_id(String _id){
        this._id = _id;
    }

    /**
     * <p>
     *     this method is used to return email
     * </p>
     * @return email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * <p>
     *     this method is used to set email
     * </p>
     * @param email email passed as String
     */
    public void setEmail(String email){
        this.email = email;
    }

    /**
     * <p>
     *     this method is used to return fullName
     * </p>
     * @return fullName
     */
    public String getFullName() {
        return this.fullName;
    }

    /**
     * <p>
     *     this method is used to set fullName
     * </p>
     * @param fullName fullName passed as String
     */
    public void setFullName(String fullName){
        this.fullName = fullName;
    }

}
