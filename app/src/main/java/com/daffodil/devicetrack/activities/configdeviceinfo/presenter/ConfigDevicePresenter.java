package com.daffodil.devicetrack.activities.configdeviceinfo.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.configdeviceinfo.bl.ConfigDevBL;
import com.daffodil.devicetrack.activities.configdeviceinfo.model.DeviceCredentialsModel;
import com.daffodil.devicetrack.activities.configdeviceinfo.view.iConfigureDeviceInfo;
import com.daffodil.devicetrack.helpers.SimpleDialog;

public class ConfigDevicePresenter implements iConfigDevicePresenter {
    private iConfigureDeviceInfo activity;
    private Context context;
    private ConfigDevBL bl;
    private ProgressDialog progressDialog;

    public ConfigDevicePresenter(iConfigureDeviceInfo activity, Context context){
        this.activity = activity;
        this.context = context;
        bl = new ConfigDevBL(this, context);
    }

    /**
     * <p>
     *     Returns the consumer friendly device name
     *     This method is used to generate a friendly Device name
     *     Please note this code is copied from google
     *     dont blame me.
     * </p>
     * @return
     */
    public String getDeviceName(){
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    /**
     * <p>
     *     This method is used to capitalize the Device Model name returned form
     *     {@link #getDeviceName() getDeviceName} method.
     *     Note this method is also copied from google
     *     please dont blame me for this either
     * </p>
     * @param str string to capitalize
     * @return capitalized string.
     */
    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

//        String phrase = "";
        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
//                phrase += Character.toUpperCase(c);
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
//            phrase += c;
            phrase.append(c);
        }

        return phrase.toString();
    }

    /**
     * <p>
     *     This method is used to call register device api created by a mean person.
     *     This api register the device when it is configured for first time.
     *     Please note that a device can not be registered more than once. It returns error.
     *     Please call delete api before registering it again.
     * </p>
     * @param macAddress Mac address of the system
     * @param deviceName device name in daffodil format like DAFFOMOB-05
     * @param modelNumber model number of device
     * @param token token received after success of admin login api
     */
    public void registerDevice(String macAddress, String deviceName, String modelNumber, String token){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Registering device.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        bl.tryRegisterDevice(macAddress, deviceName, modelNumber, token);
    }

    @Override
    public void displayErrorMessage(String message) {
        progressDialog.dismiss();
        SimpleDialog.getSimpleDialog(context.getString(R.string.alert_label), message, context).show();
        activity.registerationFailed();
    }

    @Override
    public void onRegisterSuccessFul(DeviceCredentialsModel model) {
        progressDialog.dismiss();
        activity.registerationSuccessFul(model);
    }

    @Override
    public void onUpdateSuccessFul(DeviceCredentialsModel model) {
        progressDialog.dismiss();
        activity.updateSuccessFull(model)     ;
    }

    /**
     * <p>
     *     This method calls update device api.
     *     This api can update the device name of daffodil naming of a device.
     *     Please note that update api url is device/:id not devices/:mac_address or whatever
     *     please have some courtesy
     * </p>
     * @param macAddress the mac address
     * @param deviceName the new device name
     * @param modelNumber the model number duh
     * @param token the token received form admin login api
     * @param deviceId device id whose device name is to be changed saved in sP
     */
    public void updateDevice(String macAddress, String deviceName, String modelNumber, String token, String deviceId) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Updating device name.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        bl.tryUpdateDevice(macAddress, deviceName, modelNumber, token, deviceId);
    }

    /**
     * <p>
     *     This method checks whether the device number is empty or not
     * </p>
     * @param deviceName
     */
    public void validateDeviceName(String deviceName) {
        if(deviceName.equals("")){
            activity.onInvalid();
        }else{
            activity.onValid();
        }
    }
}
