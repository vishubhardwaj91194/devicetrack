package com.daffodil.devicetrack.activities.viewdevices.model;

import java.util.ArrayList;

public class ViewDevicesModel {
    public int skip;
    public int limit;
    public ArrayList<ViewDeviceDetail> deviceDetails;
    public int totalCount;
    public boolean hasPrev;
    public boolean hasNext;
    public int itemCount;
}
