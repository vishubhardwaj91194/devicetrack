package com.daffodil.devicetrack.activities.viewdevices.model;

public class ViewDetailsData {
    public String deviceName, stateName, ownerName, projectName, managerName;
    public int viewType;
    public String id;
}
