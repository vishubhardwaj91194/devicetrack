package com.daffodil.devicetrack.activities.empdetails.view;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.empdetails.model.FreeDeviceModel;
import com.daffodil.devicetrack.activities.empdetails.presenter.EmpDetailPresenter;
import com.daffodil.devicetrack.activities.empdeviceform.model.EmpDeviceData;
import com.daffodil.devicetrack.activities.homescreen.view.MainActivity;
import com.daffodil.devicetrack.helpers.AppPreferences;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EmpDetail extends AppCompatActivity implements View.OnClickListener, iEmpDetail {


    @Bind(R.id.asset_id)
    TextView assetID;

    @Bind(R.id.employee_code)
    TextView employeeCode;

    @Bind(R.id.employee_name)
    TextView employeeName;

    @Bind(R.id.manager_name)
    TextView managerName;

    @Bind(R.id.project_name)
    TextView projectName;

    @Bind(R.id.btn_free_device)
    Button freeDevice;

    private EmpDetailPresenter presenter;
    private FreeDeviceModel model;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_detail);
        ButterKnife.bind(this);

        EmpDeviceData storedData = AppPreferences.getTakeDeviceData(this);
        assetID.setText(AppPreferences.getAssetId(this));
        employeeCode.setText(storedData.empCode);
        employeeName.setText(storedData.empName);
        managerName.setText(storedData.managerName);
        projectName.setText(storedData.project);

        freeDevice.setOnClickListener(this);

       presenter = new EmpDetailPresenter(this, this);
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

    @Override
    public void onClick(View v) {
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        String systemMacAddress = wInfo.getMacAddress();

        presenter.freeDevice(systemMacAddress);
    }

    @Override
    public void onFreeDeviceSuccessful(FreeDeviceModel model) {
        this.model = model;
        AppPreferences.clearTakeDeviceData(this);
        Intent main = new Intent();
        main.setClass(this, MainActivity.class);
        main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
    }

    @Override
    public void onError() {

    }
}
