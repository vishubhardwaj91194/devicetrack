package com.daffodil.devicetrack.activities.adminloginscreen.view;

import com.daffodil.devicetrack.activities.adminloginscreen.model.AdminCredentialModel;

/**
 * This interface is used to declare all the methods that are implented in {@link AdminLogin} and
 * called from {@link com.daffodil.devicetrack.activities.adminloginscreen.presenter.AdminLoginPresenter}
 */
public interface iAdminLogin {

    /**
     * This method is called from {@link com.daffodil.devicetrack.activities.adminloginscreen.presenter.AdminLoginPresenter}
     * when username or passwords are empty while validation
     */
    public void onEmptyFields();

    /**
     * This method is called form {@link com.daffodil.devicetrack.activities.adminloginscreen.presenter.AdminLoginPresenter}
     * when authentication fails. ie username password did not match.
     */
    public void onAuthenticationFailed();

    /**
     * This method is called form {@link com.daffodil.devicetrack.activities.adminloginscreen.presenter.AdminLoginPresenter}
     * when authentication us successful. ie username password combination is found in the database while querying the api.
     * good?
     */
    public void onAuthenticationSuccessFul(AdminCredentialModel model);
}
