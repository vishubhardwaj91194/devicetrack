package com.daffodil.devicetrack.activities.viewdevices.presenter;

import com.daffodil.devicetrack.activities.viewdevices.model.ViewDevicesModel;

public interface iViewDevicesPres {
    public void onResponce(ViewDevicesModel model);
    public void onError(String error);

    void displayErrorMessage(String message);

    void onDeletResponce(int position);
}
