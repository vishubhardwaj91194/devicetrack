package com.daffodil.devicetrack.activities.empdeviceform.view;

import com.daffodil.devicetrack.activities.empdeviceform.model.BookDeviceModel;

public interface iEmpDeviceForm {
    public void onDeviceBooked(BookDeviceModel model);
    public void onError();

    public void onValidateSuccess(String empCodeText, String empNametext, String managerNameText, String projNametext);
    public void onValidateError(String message);
}
