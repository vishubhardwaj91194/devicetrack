package com.daffodil.devicetrack.activities.viewdevices.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.viewdevices.model.ViewDetailsData;
import com.daffodil.devicetrack.activities.viewdevices.model.ViewDeviceDetail;
import com.daffodil.devicetrack.activities.viewdevices.model.ViewDevicesModel;
import com.daffodil.devicetrack.activities.viewdevices.presenter.ViewDevicesPresenter;
import com.daffodil.devicetrack.helpers.AppConst;
import com.daffodil.devicetrack.helpers.AppPreferences;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ViewDevices extends AppCompatActivity implements  CellClick, iViewDevices {

    @Bind(R.id.view_device_recycler_view)
    RecyclerView viewDeviceRecyclerView;

    private ViewDeviceAdapter adapter;
    private ArrayList<ViewDetailsData> data;
    private ViewDevicesPresenter presenter;
    private ViewDevicesModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_devices);
        ButterKnife.bind(this);

        viewDeviceRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        viewDeviceRecyclerView.setLayoutManager(mLayoutManager);

        data = new ArrayList<>();

        presenter = new ViewDevicesPresenter(this, this);
        presenter.viewDevicesData();
    }

    @Override
    public void onCellClick(int position) {
        String deviceId = data.get(position).id;
        presenter.deleteDevice(deviceId, position);
    }

    @Override
    public void loadMore() {
        if(model.hasNext){
            data.remove((data.size()-1));
        }
        int skip = data.size();
        presenter.loadMoreData(skip);
    }

    @Override
    public void onResponce(ViewDevicesModel model) {
        this.model = model;

        for (ViewDeviceDetail details:model.deviceDetails) {
            ViewDetailsData level = new ViewDetailsData();
            level.deviceName = details.name;
            level.id = details._id;
            if(details.owner == null){
                level.stateName = "FREE";
                level.viewType = AppConst.VIEW_TYPE_FREE;
            }else{
                level.stateName = "In Use";
                level.viewType = AppConst.VIEW_TYPE_IN_USE;
                level.ownerName = details.owner.name;
                level.projectName = details.owner.project;
                level.managerName = details.owner.manager;
            }
            data.add(level);
        }

        if(model.hasNext) {
            ViewDetailsData level1 = new ViewDetailsData();
            level1.viewType = AppConst.VIEW_TYPE_LOAD_MORE;
            data.add(level1);
        }

        adapter = new ViewDeviceAdapter(data, getApplicationContext(), this);
        viewDeviceRecyclerView.setAdapter(adapter);
    }

    @Override
    public void onError() {
        finish();
    }

    @Override
    public void deleteResponce(int position) {
        if(data.get(position).deviceName.equals(AppPreferences.getAssetId(this))){
            AppPreferences.clearAssetId(this);
        }
        data.remove(position);
        adapter.resetData(data);
        adapter.notifyDataSetChanged();

    }
}
