package com.daffodil.devicetrack.activities.empdeviceform.view;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.empdetails.view.EmpDetail;
import com.daffodil.devicetrack.activities.empdeviceform.model.BookDeviceModel;
import com.daffodil.devicetrack.activities.empdeviceform.model.EmpDeviceData;
import com.daffodil.devicetrack.activities.empdeviceform.presenter.EmpDevPresenter;
import com.daffodil.devicetrack.helpers.AppPreferences;
import com.daffodil.devicetrack.helpers.SimpleDialog;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EmpDeviceForm extends AppCompatActivity implements View.OnClickListener, iEmpDeviceForm {

    @Bind(R.id.asset_id)
    TextView assetId;

    @Bind(R.id.employee_code)
    EditText employeeCode;

    @Bind(R.id.employee_name)
    EditText employeeName;

    @Bind(R.id.manager_name)
    EditText managerName;

    @Bind(R.id.project_name)
    EditText projectName;

    @Bind(R.id.save_emp_device_form_btn)
    Button btnSave;

    private EmpDeviceData prefData;
    private EmpDevPresenter presenter;
    private BookDeviceModel model;

    private boolean previousClicks = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_device_form);
        ButterKnife.bind(this);

        assetId.setText(AppPreferences.getAssetId(this));
        btnSave.setOnClickListener(this);

        presenter = new EmpDevPresenter(this, this);
    }

    @Override
    public void onClick(View v) {
        if(!previousClicks && v.getId()==R.id.save_emp_device_form_btn){
            String empCodeText = employeeCode.getText().toString().trim();
            String empNametext = employeeName.getText().toString().trim();
            String managerNameText = managerName.getText().toString().trim();
            String projNametext = projectName.getText().toString().trim();

            presenter.validate(empCodeText, empNametext, managerNameText, projNametext);
        }
    }

    @Override
    public void onDeviceBooked(BookDeviceModel model) {
        this.model = model;
        AppPreferences.setTakeDeviceData(prefData, this);

        Intent nextActivity = new Intent();
        nextActivity.setClass(this, EmpDetail.class);
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(nextActivity);
    }

    @Override
    public void onError() {
        previousClicks = false;
    }

    @Override
    public void onValidateSuccess(String empCodeText, String empNametext, String managerNameText, String projNametext) {
        prefData = new EmpDeviceData();
        prefData.empCode = "DFG-"+empCodeText;
        empNametext = capitalizeString(empNametext);
        prefData.empName = empNametext;
        managerNameText = capitalizeString(managerNameText);
        prefData.managerName = managerNameText;
        projNametext = capitalizeString(projNametext);
        prefData.project = projNametext;

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        String systemMacAddress = wInfo.getMacAddress();

        previousClicks = true;
        presenter.bookDevice(systemMacAddress, empCodeText, prefData.empName, prefData.managerName, prefData.project);
    }

    @Override
    public void onValidateError(String message) {
        SimpleDialog.getSimpleDialog(getString(R.string.alert_label), message, this).show();
    }

    private String capitalizeString(String string) {
        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        for (int i = 0; i < chars.length; i++) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
                found = true;
            } else if (Character.isWhitespace(chars[i]) || chars[i]=='.' || chars[i]=='\'') { // You can add other chars here
                found = false;
            }
        }
        return String.valueOf(chars);
    }
}
