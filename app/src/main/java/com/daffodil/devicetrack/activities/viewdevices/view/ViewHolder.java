package com.daffodil.devicetrack.activities.viewdevices.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ViewHolder extends RecyclerView.ViewHolder {
    public ViewFree viewFree;
    public ViewInUse viewInUse;
    public ViewLoadMore viewLoadMore;

    public ViewHolder(View itemView) {
        super(itemView);

        if(itemView instanceof ViewFree) {
            viewFree = (ViewFree) itemView;
        } else if(itemView instanceof ViewInUse) {
            viewInUse = (ViewInUse) itemView;
        } else if(itemView instanceof ViewLoadMore) {
            viewLoadMore = (ViewLoadMore) itemView;
        }
    }
}
