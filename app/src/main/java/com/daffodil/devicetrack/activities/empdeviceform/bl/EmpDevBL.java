package com.daffodil.devicetrack.activities.empdeviceform.bl;

import android.content.Context;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.empdeviceform.model.BookDeviceModel;
import com.daffodil.devicetrack.activities.empdeviceform.presenter.iEmpDevPresenter;
import com.daffodil.devicetrack.helpers.AppConst;
import com.daffodil.devicetrack.helpers.DataHandler;
import com.daffodil.devicetrack.helpers.ServiceCallManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class EmpDevBL implements iEmpDevBl {
    private iEmpDevPresenter presenter;
    private Context context;
    public EmpDevBL(iEmpDevPresenter presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    public void tryBookDevice(String mac, String empCode, String name, String manager, String project) {
        String url = AppConst.BASE_URL + "/device/" + mac;
        Map<String, String> simpleHeader = DataHandler.getSimpleHeaders();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("empCode", Integer.parseInt(empCode));
            jsonObject.put("name", name);
            jsonObject.put("manager", manager);
            jsonObject.put("project", project);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ServiceCallManager.sendBookRequestWithVolley(url, simpleHeader, jsonObject, context, this);
    }

    @Override
    public void onResponce(String data) {
        JSONObject responce = null;
        try {
            responce = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        BookDeviceModel model = new BookDeviceModel();
        model._id = responce.optString(AppConst.VIEW_DEVICE_KEY_7);
        model.name = responce.optString(AppConst.VIEW_DEVICE_KEY_5);
        model.model = responce.optString(AppConst.VIEW_DEVICE_KEY_6);
        model.macAddress = responce.optString(AppConst.VIEW_DEVICE_KEY_4);
        model.owner = responce.optString(AppConst.VIEW_DEVICE_KEY_8);
        presenter.onResponce(model);
    }

    @Override
    public void onError(String s) {
        presenter.onError(context.getString(R.string.unknown_error_message));
    }
}
