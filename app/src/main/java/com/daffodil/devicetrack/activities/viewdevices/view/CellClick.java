package com.daffodil.devicetrack.activities.viewdevices.view;

public interface CellClick {
    public void onCellClick(int position);
    public void loadMore();
}
