package com.daffodil.devicetrack.activities.viewdevices.bl;

import android.content.Context;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.viewdevices.presenter.iViewDevicesPres;
import com.daffodil.devicetrack.helpers.AppConst;
import com.daffodil.devicetrack.helpers.AppPreferences;
import com.daffodil.devicetrack.helpers.DataHandler;
import com.daffodil.devicetrack.helpers.JSONParser;
import com.daffodil.devicetrack.helpers.ServiceCallManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class ViewDeviceBL implements iViewDeviceBL {
    private iViewDevicesPres presenter;
    private Context context;
    public ViewDeviceBL(iViewDevicesPres presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    public void tryViewDevicesDownload(int skip, int limit) {
        String url = AppConst.BASE_URL + "/devices?skip=" + skip + "&limit=" + limit;
        Map<String, String> simpleHeader = DataHandler.getSimpleHeaders();
        ServiceCallManager.sendViewDeviceRequestWithVolley(url, simpleHeader, this.context, this);
    }

    @Override
    public void onResponce(String data) {
        presenter.onResponce(JSONParser.getViewResponceToModel(data));
    }

    @Override
    public void onError(String s) {
        presenter.onError(context.getString(R.string.unknown_error_message));
    }

    @Override
    public void onDeleteResponce(String response, int position) {
        presenter.onDeletResponce(position);
    }

    @Override
    public void onDeleteError(String s) {
        JSONObject errorResponce = null;
        try {
            errorResponce = new JSONObject(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = context.getString(R.string.unknown_error_message);
        if(errorResponce == null) {
            message = context.getString(R.string.unknown_error_message);
        } else {
            String errorCode = "";
            try {
                errorCode = errorResponce.getString(context.getString(R.string.errorCode));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(errorCode.equals(context.getString(R.string.invalidDeviceName))) {
                message = AppConst.INVALID_DEVICE;
            }/*else if(errorCode.equals(context.getString(R.string.invalidLogin))) {
                message = AppConst.INVALID_CREDENTIALS;
            }*/
        }
        presenter.displayErrorMessage(message);
    }

    @Override
    public void onOtherError(String s) {
        presenter.displayErrorMessage(context.getString(R.string.unknown_error_message));
    }

    public void tryDeleteDevice(String id, int position) {
        String url = AppConst.BASE_URL + "/devices/" + id;
        String token = AppPreferences.getToken(context);
        Map<String, String> complexHeader = DataHandler.getCompleHandler(token);
        ServiceCallManager.sendDeleteDeviceRequestWithVolley(url, complexHeader, context, this, position);
    }
}
