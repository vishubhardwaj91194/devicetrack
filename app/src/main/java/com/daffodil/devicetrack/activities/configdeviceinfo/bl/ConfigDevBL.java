package com.daffodil.devicetrack.activities.configdeviceinfo.bl;

import android.content.Context;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.configdeviceinfo.model.DeviceCredentialsModel;
import com.daffodil.devicetrack.activities.configdeviceinfo.presenter.iConfigDevicePresenter;
import com.daffodil.devicetrack.helpers.AppConst;
import com.daffodil.devicetrack.helpers.ServiceCallManager;

import org.json.JSONException;
import org.json.JSONObject;

public class ConfigDevBL implements iConfigDevBL {
    private iConfigDevicePresenter presenter;
    private Context context;
    public ConfigDevBL(iConfigDevicePresenter presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    public void tryRegisterDevice(String macAddress, String deviceName, String modelNumber, String token) {
        ServiceCallManager.sendRegisterDeviceRequestWithVolley(macAddress, deviceName, modelNumber, token, this, context);
    }

    @Override
    public void onError(String error) {
        JSONObject errorResponce = null;
        try {
            errorResponce = new JSONObject(error);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = context.getString(R.string.unknown_error_message);
        if(errorResponce == null) {
            message = context.getString(R.string.unknown_error_message);
        } else {
            String errorCode = "";
            try {
                errorCode = errorResponce.getString(context.getString(R.string.errorCode));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(errorCode.equals(AppConst.REGISTER_ERROR_KEY_1)) {

                message = AppConst.DEVICE_ALREADY_REGISTERED;
            }else if(errorCode.equals(context.getString(R.string.invalidInput))) {
                message = AppConst.INVALID_UPDATE_ID;
            }

        }
        presenter.displayErrorMessage(message);
    }

    @Override
    public void onResponce(String data) {
        JSONObject responce = null, auth = null, admin = null;
        try {
            responce = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        DeviceCredentialsModel model = new DeviceCredentialsModel();
        try {
            model.macAddress = responce.getString(AppConst.REGISTER_BODY_1);
            model.name = responce.getString(AppConst.REGISTER_BODY_2);
            model.model  = responce.getString(AppConst.REGISTER_BODY_3);
            model._id = responce.getString(AppConst.ADMIN_LOGIN_KEY_4);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        presenter.onRegisterSuccessFul(model);
    }

    @Override
    public void onUpdateResponce(String data) {
        JSONObject responce = null, auth = null, admin = null;
        try {
            responce = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        DeviceCredentialsModel model = new DeviceCredentialsModel();
        try {
            model.macAddress = responce.getString(AppConst.REGISTER_BODY_1);
            model.name = responce.getString(AppConst.REGISTER_BODY_2);
            model.model  = responce.getString(AppConst.REGISTER_BODY_3);
            //model._id = responce.getString(AppConst.ADMIN_LOGIN_KEY_4);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        presenter.onUpdateSuccessFul(model);
    }

    public void tryUpdateDevice(String macAddress, String deviceName, String modelNumber, String token, String deviceId) {
        ServiceCallManager.sendUpdateDeviceRequestWithVilley(macAddress, deviceName, modelNumber, token, this, context, deviceId);
    }
}
