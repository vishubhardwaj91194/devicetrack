package com.daffodil.devicetrack.activities.viewdevices.bl;

public interface iViewDeviceBL {
    void onResponce(String response);

    void onError(String s);

    void onDeleteResponce(String response, int position);

    void onDeleteError(String s);

    void onOtherError(String s);
}
