package com.daffodil.devicetrack.activities.empdeviceform.presenter;

import com.daffodil.devicetrack.activities.empdeviceform.model.BookDeviceModel;

public interface iEmpDevPresenter {
    void onResponce(BookDeviceModel model);

    void onError(String string);
}
