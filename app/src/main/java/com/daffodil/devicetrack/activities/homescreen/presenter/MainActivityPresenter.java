package com.daffodil.devicetrack.activities.homescreen.presenter;

import android.content.Context;

import com.daffodil.devicetrack.activities.empdeviceform.model.EmpDeviceData;
import com.daffodil.devicetrack.activities.homescreen.view.MainActivity;
import com.daffodil.devicetrack.activities.homescreen.view.iMainActivity;
import com.daffodil.devicetrack.helpers.AppPreferences;

public class MainActivityPresenter {

    /**
     * <p>
     *     Stored activity object in form of {@link iMainActivity} interface for callback objects.
     * </p>
     */
    private iMainActivity activity;
    /**
     * <p>
     *     Stored application context for getting data from shared preferences.
     * </p>
     */
    private Context context;

    /**
     * <p>
     *      public constructor for the presenter.
     * </p>
     * @param activity the instance of {@link MainActivity} passed as this
     * @param context application context
     */
    public MainActivityPresenter(iMainActivity activity, Context context){
        this.activity = activity;
        this.context = context;
    }

    /**
     * <p>
     *     This method checks whether some user have taken device. It is checked using {@link android.content.SharedPreferences}.
     *     EmpDeviceDate is stored or not. If yes {@link MainActivity#onDeviceAlreadyTaken() onDeviceAlreadyTaken} is called.
     * </p>
     */
    public void checkDevicetaken() {
        EmpDeviceData storedData = AppPreferences.getTakeDeviceData(context);
        if(storedData.empCode != null && storedData.empName != null && storedData.managerName != null && storedData.project != null){
            this.activity.onDeviceAlreadyTaken();
        }
    }
}
