package com.daffodil.devicetrack.activities.adminloginscreen.presenter;

import com.daffodil.devicetrack.activities.adminloginscreen.model.AdminCredentialModel;

/**
 * <p>
 *     This interface is used to interact with the business logic of {@link com.daffodil.devicetrack.activities.adminloginscreen.view.AdminLogin}
 *     It allows the business logic class to interact with or call method of {@link AdminLoginPresenter}.
 *
 * </p>
 */
public interface iAdminLoginPresenter {

    /**
     * <p>
     *     this method is used to display error message on {@link android.app.DialogFragment} passed as Sring
     * </p>
     * @param errorMessage  error message to be displayed on DialogFragment as String
     */
    public void displayErrorMessage(String errorMessage);

    /**
     * <p>
     *     This method is used to forward intent to {@link com.daffodil.devicetrack.activities.adminpannelscreen.view.AdminPannel}
     * </p>
     * @param model object of {@link AdminCredentialModel} model class which holds data
     */
    public void onLoginSuccess(AdminCredentialModel model);
}
