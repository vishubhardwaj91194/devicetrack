package com.daffodil.devicetrack.activities.adminpannelscreen.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.configdeviceinfo.view.ConfigDeviceInfo;
import com.daffodil.devicetrack.activities.viewdevices.view.ViewDevices;
import com.daffodil.devicetrack.helpers.AppPreferences;
import com.daffodil.devicetrack.helpers.SimpleDialog;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AdminPannel extends AppCompatActivity implements iAdminPannel, View.OnClickListener {


    @Bind(R.id.btn_configure_device)
    Button btnConfigureDevice;

    @Bind(R.id.btn_view_device)
    Button btnViewDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_pannel);

        ButterKnife.bind(this);

        btnConfigureDevice.setOnClickListener(this);
        btnViewDevice.setOnClickListener(this);
        String token = AppPreferences.getToken(this);
        // clear shared preferences
        //AppPreferences.clearSharedPreferences(this);

    }

    @Override
    public void onClick(View v) {
        final Intent nextActivity = new Intent();
        switch (v.getId()) {
            case R.id.btn_configure_device:

                String storedAssetId = AppPreferences.getAssetId(this);
                if(storedAssetId == null) {
                    // clicking button for first time
                    nextActivity.setClass(this, ConfigDeviceInfo.class);
                    startActivity(nextActivity);
                } else {
                    // clicking button for second time

                    AlertDialog warningDialog = SimpleDialog.getSimpleDialog(getString(R.string.warning_label), getString(R.string.configure_device_first_time_warning)+storedAssetId+".", this);
                    warningDialog.setCancelable(false);
                    warningDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.continue_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // pass it to next activity
                            nextActivity.setClass(AdminPannel.this, ConfigDeviceInfo.class);
                            startActivity(nextActivity);
                        }
                    });
                    warningDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    warningDialog.show();
                }
                break;
            case R.id.btn_view_device:
                nextActivity.setClass(this, ViewDevices.class);
                startActivity(nextActivity);
                break;
            default:
                break;
        }
    }
}
