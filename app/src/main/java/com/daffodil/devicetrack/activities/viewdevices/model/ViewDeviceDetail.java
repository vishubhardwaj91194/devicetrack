package com.daffodil.devicetrack.activities.viewdevices.model;

public class ViewDeviceDetail {
    public String macAddress;
    public String name;
    public String model;
    public String _id;

    public Owner owner;
}
