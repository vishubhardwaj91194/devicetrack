package com.daffodil.devicetrack.activities.empdetails.presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.empdetails.bl.EmpDetailBl;
import com.daffodil.devicetrack.activities.empdetails.model.FreeDeviceModel;
import com.daffodil.devicetrack.activities.empdetails.view.iEmpDetail;
import com.daffodil.devicetrack.helpers.SimpleDialog;

public class EmpDetailPresenter implements iEmpDetailPresenter {
    private iEmpDetail activity;
    private Context context;
    private EmpDetailBl bl;
    private ProgressDialog progressDialog;

    public EmpDetailPresenter(iEmpDetail acticvity, Context context){
        this.activity = acticvity;
        this.context = context;
        bl = new EmpDetailBl(this, context);
    }

    public void freeDevice(String systemMacAddress) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Freeing device.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        bl.tryFreeDevice(systemMacAddress);
    }

    @Override
    public void onResponce(FreeDeviceModel model) {
        progressDialog.dismiss();
        activity.onFreeDeviceSuccessful(model);
    }

    @Override
    public void onError(String message) {
        progressDialog.dismiss();
        SimpleDialog.getSimpleDialog(context.getString(R.string.alert_label), message, context).show();
        activity.onError();
    }
}
