package com.daffodil.devicetrack.activities.homescreen.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.adminloginscreen.view.AdminLogin;
import com.daffodil.devicetrack.activities.empdetails.view.EmpDetail;
import com.daffodil.devicetrack.activities.empdeviceform.view.EmpDeviceForm;
import com.daffodil.devicetrack.activities.homescreen.presenter.MainActivityPresenter;
import com.daffodil.devicetrack.helpers.AppPreferences;
import com.daffodil.devicetrack.helpers.SimpleDialog;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * <p>
 * This activity is the starting point of application. This is the main activity.
 * It displays a choice of two buttons to go to Admin pannel i.e. {@link AdminLogin} activity.
 * Or to Take Device screen which was not implemented till date.
 * PS . here you let the phone when you take the device.
 * </p>
 * <p>
 * Author : Vishu Bhardwaj
 * Date : 21-09-2016
 * </p>
 */
public class MainActivity extends AppCompatActivity implements iMainActivity, View.OnClickListener {


    /**
     * Here I used butter knife to inject. Note it is used to replace writing of {@link AppCompatActivity#findViewById(int) findViewById()}.
     */

    /**
     * Button to click and go to admin login screen {@link AdminLogin}
     */
    @Bind(R.id.btn_admin)
    Button btnAdmin;

    // button to go to take device screen
    @Bind(R.id.btn_take_device)
    Button btnTakeDevice;

    private MainActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new MainActivityPresenter(this, this);
        presenter.checkDevicetaken();



        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // On click listeners to this activity
        btnAdmin.setOnClickListener(this);
        btnTakeDevice.setOnClickListener(this);
    }

    // On click method
    @Override
    public void onClick(View v) {
        // Intent to go to some other activty
        Intent nextActivity = new Intent();

        // check which button is clicked
        switch (v.getId()) {

            // Admin button is clicked
            case R.id.btn_admin:

                /**
                 * go to {@link AdminLogin} activity
                 */
                nextActivity.setClass(this, AdminLogin.class);
                startActivity(nextActivity);
                break;

            // Take device button is clicked
            case R.id.btn_take_device:
                String storedAssetId = AppPreferences.getAssetId(this);
                if (storedAssetId == null) {
                    SimpleDialog.getSimpleDialog(getString(R.string.oops_label), getString(R.string.device_not_configured), this).show();
                } else {
                    nextActivity.setClass(this, EmpDeviceForm.class);
                    startActivity(nextActivity);
                }

                break;

            // something else is clicked
            default:
                break;
        }
    }

    @Override
    public void onDeviceAlreadyTaken() {
        Intent goToEmpDetailsScreen = new Intent();
        goToEmpDetailsScreen.setClass(this, EmpDetail.class);
        goToEmpDetailsScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(goToEmpDetailsScreen);
    }
}
