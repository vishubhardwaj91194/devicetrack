package com.daffodil.devicetrack.activities.adminloginscreen.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.adminloginscreen.model.AdminCredentialModel;
import com.daffodil.devicetrack.activities.adminloginscreen.presenter.AdminLoginPresenter;
import com.daffodil.devicetrack.activities.adminpannelscreen.view.AdminPannel;
import com.daffodil.devicetrack.helpers.AppPreferences;
import com.daffodil.devicetrack.helpers.SimpleDialog;

import butterknife.Bind;
import butterknife.ButterKnife;

public class AdminLogin extends AppCompatActivity implements iAdminLogin, View.OnClickListener {

    // same butter knife
    @Bind(R.id.username)
    EditText username;

    @Bind(R.id.password)
    EditText password;

    @Bind(R.id.btn_login)
    Button btnLogin;

    // presenter
    private AdminLoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);
        ButterKnife.bind(this);

        btnLogin.setOnClickListener(this);

        // Create object of AdminLoginPresenter to interact
        presenter = new AdminLoginPresenter(this, this);
    }

    @Override
    public void onClick(View v) {
        // check iff login button is clicked
        if (v.getId() == R.id.btn_login) {

            // retreive texts of username and passoword
            String usernameText = username.getText().toString().trim();
            String passwordText = password.getText().toString().trim();

            // username and password are passed to AdminLoginPresenter fot valition and login
            presenter.validateAndAdminLogin(usernameText, passwordText);
        }
    }

    /**
     * <p>
     * This method when called displays a dialog showing message that username password can not be blank.
     * see defination at{@link iAdminLogin#onEmptyFields() here}.
     * </p>
     */
    @Override
    public void onEmptyFields() {
        SimpleDialog.getSimpleDialog(getString(R.string.alert_label), getString(R.string.empty_text_box), this).show();
    }

    /**
     * <p>
     * This method when called displays a dialog showing message that username password did not match.
     * ie authentication failed.
     * see defination at{@link iAdminLogin#onAuthenticationFailed() here}.
     * </p>
     */
    @Override
    public void onAuthenticationFailed() {
        // login failed
        SimpleDialog.getSimpleDialog(getString(R.string.alert_label), getString(R.string.admin_not_match), this).show();
    }

    /**
     * <p>
     * This method when called starts an {@link Intent} to srart {@link AdminPannel} activity.
     * see defination at{@link iAdminLogin#onAuthenticationSuccessFul(AdminCredentialModel model) here}.
     * </p>
     */
    @Override
    public void onAuthenticationSuccessFul(AdminCredentialModel model) {
        // login successful
        Intent nextActivity = new Intent(this, AdminPannel.class);
        AppPreferences.setToken(model.getToken(), this);
        startActivity(nextActivity);
        finish();
    }
}
