package com.daffodil.devicetrack.activities.viewdevices.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.daffodil.devicetrack.activities.viewdevices.model.ViewDetailsData;
import com.daffodil.devicetrack.helpers.AppConst;

import java.util.ArrayList;

public class ViewDeviceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ViewDetailsData> viewDevices;
    private Context applicationContext;
    private int length;
    private CellClick listener;

    public ViewDeviceAdapter(ArrayList<ViewDetailsData> passedArrayList, Context context, CellClick listener) {
        this.viewDevices = passedArrayList;
        this.applicationContext = context;
        this.length = this.viewDevices.size();
        this.listener = listener;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder holder = null;
        if(viewType == AppConst.VIEW_TYPE_FREE){
            holder = new ViewHolder(new ViewFree(applicationContext));
        }else if(viewType == AppConst.VIEW_TYPE_IN_USE) {
            holder = new ViewHolder(new ViewInUse(applicationContext));
        } else if(viewType == AppConst.VIEW_TYPE_LOAD_MORE) {
            holder = new ViewHolder(new ViewLoadMore(applicationContext));
        }
        return holder;
    }

    public void resetData(ArrayList<ViewDetailsData> passedData){
        this.viewDevices = passedData;
        this.length = this.viewDevices.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder myHolder = (ViewHolder) holder;
        switch (getItemViewType(position)) {
            case 0:
                myHolder.viewFree.setData(viewDevices.get(position), position);
                myHolder.viewFree.setOnClick(listener);
                break;
            case 1:
                myHolder.viewInUse.setData(viewDevices.get(position), position);
                myHolder.viewInUse.setOnClick(listener);
                break;
            case 2:
                myHolder.viewLoadMore.setOnClick(listener);
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return this.length;
    }

    @Override
    public int getItemViewType(int position) {
        int passedViewType = viewDevices.get(position).viewType;
        if(passedViewType == AppConst.VIEW_TYPE_FREE){
            return AppConst.VIEW_TYPE_FREE;
        }else if(passedViewType == AppConst.VIEW_TYPE_IN_USE){
            return AppConst.VIEW_TYPE_IN_USE;
        }else if(passedViewType == AppConst.VIEW_TYPE_LOAD_MORE) {
            return AppConst.VIEW_TYPE_LOAD_MORE;
        }
        return AppConst.VIEW_TYPE_FREE;
    }
}
