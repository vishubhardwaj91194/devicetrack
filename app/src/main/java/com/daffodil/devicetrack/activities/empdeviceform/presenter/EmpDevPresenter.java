package com.daffodil.devicetrack.activities.empdeviceform.presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.empdeviceform.bl.EmpDevBL;
import com.daffodil.devicetrack.activities.empdeviceform.model.BookDeviceModel;
import com.daffodil.devicetrack.activities.empdeviceform.view.iEmpDeviceForm;
import com.daffodil.devicetrack.helpers.SimpleDialog;

public class EmpDevPresenter implements iEmpDevPresenter {
    private iEmpDeviceForm activity;
    private Context context;
    private EmpDevBL bl;
    private ProgressDialog progressDialog;

    public EmpDevPresenter(iEmpDeviceForm activity, Context context) {
        this.activity = activity;
        this.context = context;
        bl = new EmpDevBL(this, context);
    }


    public void bookDevice(String systemMacAddress, String empCode, String empName, String managerName, String project) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Booking device.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        bl.tryBookDevice(systemMacAddress, empCode, empName, managerName, project);
    }

    @Override
    public void onResponce(BookDeviceModel model) {
        progressDialog.dismiss();
        activity.onDeviceBooked(model);
    }

    @Override
    public void onError(String message) {
        progressDialog.dismiss();
        SimpleDialog.getSimpleDialog(context.getString(R.string.alert_label), message, context).show();
        activity.onError();
    }


    public void validate(String empCodeText, String empNametext, String managerNameText, String projNametext) {
        String message = "";
        if(empCodeText.equals("")){
            message = context.getString(R.string.emp_code_empty);
        }else if(empNametext.equals("")) {
            message = context.getString(R.string.emp_name_empty);
        }else if(managerNameText.equals("")) {
            message = context.getString(R.string.man_name_empty);
        }else if(projNametext.equals("")) {
            message = context.getString(R.string.proj_name_empty);
        }

        if(!message.equals("")) {
            activity.onValidateError(message);
        } else {
            activity.onValidateSuccess(empCodeText, empNametext, managerNameText, projNametext);
        }
    }
}
