package com.daffodil.devicetrack.activities.viewdevices.model;

public class Owner {
    public String empCode;
    public String name;
    public String manager;
    public String project;
    public String _id;
}
