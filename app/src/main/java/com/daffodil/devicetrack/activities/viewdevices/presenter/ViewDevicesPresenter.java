package com.daffodil.devicetrack.activities.viewdevices.presenter;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.viewdevices.bl.ViewDeviceBL;
import com.daffodil.devicetrack.activities.viewdevices.model.ViewDevicesModel;
import com.daffodil.devicetrack.activities.viewdevices.view.iViewDevices;
import com.daffodil.devicetrack.helpers.SimpleDialog;

public class ViewDevicesPresenter implements iViewDevicesPres {
    private iViewDevices activity;
    private Context context;
    private ViewDeviceBL bl;
    private ProgressDialog progressDialog;

    public ViewDevicesPresenter(iViewDevices activity, Context context) {
        this.activity = activity;
        this.context = context;
        bl =new ViewDeviceBL(this, context);
    }


    public void viewDevicesData() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Downloading data.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        bl.tryViewDevicesDownload(0, 5);
    }

    public void loadMoreData(int skip) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Downloading data.");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        bl.tryViewDevicesDownload(skip, 10);
    }

    @Override
    public void onResponce(ViewDevicesModel model) {
        progressDialog.dismiss();
        activity.onResponce(model);
    }

    @Override
    public void onError(String errorMessage) {
        progressDialog.dismiss();
        AlertDialog dialog = SimpleDialog.getSimpleDialog(this.context.getString(R.string.alert_label), errorMessage, this.context);
        dialog.setButton(
            DialogInterface.BUTTON_POSITIVE,
            context.getString(R.string.ok_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                activity.onError();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void displayErrorMessage(String message) {
        progressDialog.dismiss();
        SimpleDialog.getSimpleDialog(this.context.getString(R.string.alert_label), message, this.context).show();
    }

    @Override
    public void onDeletResponce(int position) {
        activity.deleteResponce(position);
    }

    public void deleteDevice(String id, int position) {
        bl.tryDeleteDevice(id, position);
    }
}
