package com.daffodil.devicetrack.activities.empdetails.presenter;

import com.daffodil.devicetrack.activities.empdetails.model.FreeDeviceModel;

public interface iEmpDetailPresenter {
    void onResponce(FreeDeviceModel model);

    void onError(String string);
}
