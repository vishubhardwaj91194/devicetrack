package com.daffodil.devicetrack.activities.viewdevices.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.viewdevices.model.ViewDetailsData;

public class ViewInUse extends LinearLayout implements View.OnClickListener {
    public TextView deviceName, stateName, ownerName, projectName, managerName;
    public ImageView deleteButton;
    private Context context;
    private CellClick clickListener;
    private int position;
    public ViewInUse(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.device_inuse, this, true);
        LinearLayout viewRoot = (LinearLayout) view;

        deviceName = (TextView) viewRoot.findViewById(R.id.device_name);
        stateName = (TextView) viewRoot.findViewById(R.id.state_name);
        ownerName =  (TextView) viewRoot.findViewById(R.id.owner_name);
        projectName =  (TextView) viewRoot.findViewById(R.id.project_name);
        managerName =  (TextView) viewRoot.findViewById(R.id.manager_name);
        deleteButton = (ImageView) viewRoot.findViewById(R.id.imageView);

        deleteButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        clickListener.onCellClick(position);
    }

    public void setData(ViewDetailsData data, int pos){
        deviceName.setText(data.deviceName);
        stateName.setText(data.stateName);
        ownerName.setText(data.ownerName);
        projectName.setText(data.projectName);
        managerName.setText(data.managerName);
        this.position = pos;
    }

    public void setOnClick(CellClick listener){
        this.clickListener = listener;
    }
}
