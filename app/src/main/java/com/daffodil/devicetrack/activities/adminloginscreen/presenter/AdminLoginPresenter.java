package com.daffodil.devicetrack.activities.adminloginscreen.presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.adminloginscreen.bl.AdminLoginBL;
import com.daffodil.devicetrack.activities.adminloginscreen.model.AdminCredentialModel;
import com.daffodil.devicetrack.activities.adminloginscreen.view.iAdminLogin;
import com.daffodil.devicetrack.helpers.SimpleDialog;

public class AdminLoginPresenter implements iAdminLoginPresenter {

    private iAdminLogin adminLoginActivity;
    private AdminLoginBL adminLoginBL;
    private Context context1;
    private ProgressDialog progressDialog;

    /**
     * <p>
     *     Public constructor called form {@link com.daffodil.devicetrack.activities.adminloginscreen.view.AdminLogin} to create presenter.
     * </p>
     * @param activity {@link iAdminLogin} listener instance passed as this form {@link com.daffodil.devicetrack.activities.adminloginscreen.view.AdminLogin}
     * @param context application context
     */
    public AdminLoginPresenter(iAdminLogin activity, Context context) {
        this.adminLoginActivity = activity;
        context1 = context;
        adminLoginBL = new AdminLoginBL(this, context);
    }

    /**
     * This method is used to validate Admin login credentials and also try authentication.
     * That it.
     *
     * @param username username string
     * @param password password string
     */
    public void validateAndAdminLogin(String username, String password) {
        if (username.equals("") || password.equals("")) {
            adminLoginActivity.onEmptyFields();
        } else {
            progressDialog = new ProgressDialog(context1);
            progressDialog.setMessage("Checking credentials.");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.show();
            adminLoginBL.tryLogin(username, password);
        }/* if (username.equals("admin") && password.equals("admin")) {
            adminLoginActivity.onAuthenticationSuccessFul();
        } else {
            adminLoginActivity.onAuthenticationFailed();
        }*/
    }

    /**
     * <p>
     *     this method is used to display error message when an error occurs as {@link android.app.DialogFragment} on the string passed.
     * </p>
     * @param errorMessage a {@link String} representing text to be displayed as dialog when an error occurs
     */
    @Override
    public void displayErrorMessage(String errorMessage) {
        progressDialog.dismiss();
        SimpleDialog.getSimpleDialog(context1.getString(R.string.alert_label), errorMessage, context1).show();
    }

    /**
     * <p>
     *     this method is called when login api returns success.
     *     the method simply calls {@link com.daffodil.devicetrack.activities.adminloginscreen.view.AdminLogin#onAuthenticationSuccessFul(AdminCredentialModel) onAuthenticationSuccessFul()}.
     * </p>
     * @param model the data in form of a {@link AdminCredentialModel} model object
     */
    @Override
    public void onLoginSuccess(AdminCredentialModel model) {
        progressDialog.dismiss();
        adminLoginActivity.onAuthenticationSuccessFul(model);
    }
}
