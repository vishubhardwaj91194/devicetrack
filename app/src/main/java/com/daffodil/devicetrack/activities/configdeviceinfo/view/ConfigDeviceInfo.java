package com.daffodil.devicetrack.activities.configdeviceinfo.view;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.configdeviceinfo.model.DeviceCredentialsModel;
import com.daffodil.devicetrack.activities.configdeviceinfo.presenter.ConfigDevicePresenter;
import com.daffodil.devicetrack.helpers.AppPreferences;
import com.daffodil.devicetrack.helpers.SimpleDialog;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ConfigDeviceInfo extends AppCompatActivity implements View.OnClickListener, iConfigureDeviceInfo {


    @Bind(R.id.daffo_spinner)
    Spinner daffoSpinner;

    @Bind(R.id.state)
    EditText deviceState;

    @Bind(R.id.mac_address)
    EditText macAddressEditText;

    @Bind(R.id.btn_save_config_device)
    Button btnSave;

    @Bind(R.id.daffo_num)
    EditText daffoNum;

    private ConfigDevicePresenter presenter;
    private String systemMacAddress;
    private String savedDaffoMobLabel;
    private String deviceId;
    private String number;
    private String daffoSpinnerText;
    private String daffoNumText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_device_info);
        ButterKnife.bind(this);

        presenter = new ConfigDevicePresenter(this, this);

        ArrayAdapter<CharSequence> daffoSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.daffo_array_spinner, android.R.layout.simple_spinner_item);
        daffoSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daffoSpinner.setAdapter(daffoSpinnerAdapter);

        btnSave.setOnClickListener(this);

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        systemMacAddress = wInfo.getMacAddress();

        deviceState.setText(getString(R.string.idle_message));
        macAddressEditText.setText(systemMacAddress);

        savedDaffoMobLabel = AppPreferences.getAssetId(this);
        if(savedDaffoMobLabel == null){
            // no data stored
            daffoNum.setText("");
            number = "";
        }else{
            deviceId = AppPreferences.getDeviceId(this);
            // data stored

            if(savedDaffoMobLabel.contains("DAFFOIPOD")){
                number = savedDaffoMobLabel.substring(10);
                daffoSpinner.setSelection(0);
            }else{
                number = savedDaffoMobLabel.substring(9);
                if(savedDaffoMobLabel.contains("DAFFOMOB")){
                    daffoSpinner.setSelection(1);
                }else{
                    daffoSpinner.setSelection(2);
                }
            }
            daffoNum.setText(number);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_save_config_device) {
            daffoSpinnerText = daffoSpinner.getSelectedItem().toString();
            daffoNumText = daffoNum.getText().toString().trim();
            presenter.validateDeviceName(daffoNumText);
        }
    }

    @Override
    public void registerationFailed() {
        // registation failed
        daffoSpinner.setSelection(0);
        daffoNum.setText(number);
    }

    @Override
    public void registerationSuccessFul(DeviceCredentialsModel model) {
        AppPreferences.setAssetId(model.name, this);
        AppPreferences.setDeviceId(model._id, this);
        savedDaffoMobLabel = model.name;
        deviceId = model._id;
        SimpleDialog.getSimpleDialog(getString(R.string.congratulation_label), getString(R.string.asset_id_set_message), this).show();
    }

    @Override
    public void updateSuccessFull(DeviceCredentialsModel model) {
        AppPreferences.setAssetId(model.name, this);
        savedDaffoMobLabel = model.name;
        SimpleDialog.getSimpleDialog(getString(R.string.congratulation_label), getString(R.string.device_updated_message), this).show();
    }

    @Override
    public void onValid() {
        // non empty number
        int daffoMobileNumber = Integer.parseInt(daffoNumText);
        String daffoMobileTotalString = "DAFFO"+daffoSpinnerText+"-"+daffoMobileNumber;

        // get login token from shared preferences
        String token = AppPreferences.getToken(this);
        String deviceName = presenter.getDeviceName();
        if(savedDaffoMobLabel == null){
            // Device not registered
            presenter.registerDevice(systemMacAddress, daffoMobileTotalString, deviceName, token);
        } else {
            presenter.updateDevice(systemMacAddress, daffoMobileTotalString, deviceName, token, deviceId);
        }
    }

    @Override
    public void onInvalid() {
        // empty number
        SimpleDialog.getSimpleDialog(getString(R.string.alert_label), getString(R.string.cunfig_number_empty), this).show();
    }
}
