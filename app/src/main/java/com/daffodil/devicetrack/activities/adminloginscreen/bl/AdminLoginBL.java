package com.daffodil.devicetrack.activities.adminloginscreen.bl;

import android.content.Context;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.adminloginscreen.model.AdminCredentialModel;
import com.daffodil.devicetrack.activities.adminloginscreen.presenter.iAdminLoginPresenter;
import com.daffodil.devicetrack.helpers.AppConst;
import com.daffodil.devicetrack.helpers.JSONParser;
import com.daffodil.devicetrack.helpers.ServiceCallManager;

import org.json.JSONException;
import org.json.JSONObject;

public class AdminLoginBL implements iAdminLoginBl {
    private iAdminLoginPresenter presenter;
    private Context context;

    /**
     * <p>
     *     Business logic constructor called by {@link com.daffodil.devicetrack.activities.adminloginscreen.presenter.AdminLoginPresenter}.
     *     To create Business logic object through which presenter calls Apis and get result.
     * </p>
     * @param presenter {@link iAdminLoginPresenter} listner used to callbacks
     * @param context application context
     */
    public AdminLoginBL(iAdminLoginPresenter presenter, Context context){
        this.presenter = presenter;
        this.context = context;
    }

    /**
     * <p>
     *     This method is used to call admin login api. and result us passed via {@link #onResponce(String)} or {@link #onError(String)}
     * </p>
     * @param username email address
     * @param password password
     */
    public void tryLogin(String username, String password) {

//        /*JSONObject loginRespomce = */ServiceCallManager.sendLoginRequest(username, password, context, this);
        ServiceCallManager.sendLoginRequestWithVolly(username, password, context, this);
    }

    /**
     * <p>
     *     This method is called when api responce comes from {@link ServiceCallManager}.
     *     The method converts the responce to {@link JSONObject} and passes it to {@link com.daffodil.devicetrack.activities.adminloginscreen.presenter.AdminLoginPresenter#onLoginSuccess(AdminCredentialModel) onLoginSuccess()}.
     * </p>
     * @param data the json data in string format
     */
    @Override
    public void onResponce(String data) {
        presenter.onLoginSuccess(JSONParser.getLoginResponceToModel(data));
    }

    /**
     * <p>
     *     This method is called when api call to admin login return some error responce.
     *     Basically this method parses the error and return a simple string message to
     *     {@link com.daffodil.devicetrack.activities.adminloginscreen.presenter.AdminLoginPresenter#displayErrorMessage(String) displayErrorMessage()}.
     * </p>
     * @param error the total json error in string
     */
    @Override
    public void onError(String error) {
        JSONObject errorResponce = null;
        try {
            errorResponce = new JSONObject(error);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String message = context.getString(R.string.unknown_error_message);
        if(errorResponce == null) {
            message = context.getString(R.string.unknown_error_message);
        } else {
            String errorCode = "";
            try {
                errorCode = errorResponce.getString(context.getString(R.string.errorCode));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(errorCode.equals(context.getString(R.string.invalidInput))) {
                message = AppConst.INVALID_LOGIN_MESSAGE;
            }else if(errorCode.equals(context.getString(R.string.invalidLogin))) {
                message = AppConst.INVALID_CREDENTIALS;
            }
        }
        presenter.displayErrorMessage(message);
    }
}
