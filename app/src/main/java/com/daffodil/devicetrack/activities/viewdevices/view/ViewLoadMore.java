package com.daffodil.devicetrack.activities.viewdevices.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.daffodil.devicetrack.R;

public class ViewLoadMore extends RelativeLayout implements View.OnClickListener {
    private Button btnLoadMore;
    private Context context;
    private CellClick clickListener;
    public ViewLoadMore(Context context){
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.load_more, this);
        RelativeLayout viewRoot = (RelativeLayout) view;

        btnLoadMore = (Button) viewRoot.findViewById(R.id.load_more_button);
        btnLoadMore.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.clickListener.loadMore();
    }

    public void setOnClick(CellClick listener){
        this.clickListener = listener;
    }
}
