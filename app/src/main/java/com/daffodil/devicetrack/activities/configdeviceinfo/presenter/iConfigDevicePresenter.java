package com.daffodil.devicetrack.activities.configdeviceinfo.presenter;

import com.daffodil.devicetrack.activities.configdeviceinfo.model.DeviceCredentialsModel;

/**
 * <p>
 *     This interface is used to provide callback between
 *     {@link com.daffodil.devicetrack.activities.configdeviceinfo.bl.ConfigDevBL} business logic
 *     to {@link ConfigDevicePresenter} presenter.
 * </p>
 */
public interface iConfigDevicePresenter {

    /**
     * <p>
     *     This method is called when api calls returns some error message.
     *     This method displays a simple explainable string corresponding to each message code.
     * </p>
     * @param message The error message in json string
     */
    void displayErrorMessage(String message);

    /**
     * <p>
     *     This method is called when api call to register device returns success and data returned
     *     is then converted to {@link DeviceCredentialsModel} model object and passed here. we simply
     *     store the data in shared preferences.
     * </p>
     * @param model the object of {@link DeviceCredentialsModel} model class
     */
    void onRegisterSuccessFul(DeviceCredentialsModel model);

    /**
     * <p>
     *     This method is called when api call to update device returns success and data returned
     *     is then converted to {@link DeviceCredentialsModel} model object and passed here. we simply
     *     store the data in shared preferences.
     * </p>
     * @param model the object of {@link DeviceCredentialsModel} model class
     */
    void onUpdateSuccessFul(DeviceCredentialsModel model);
}
