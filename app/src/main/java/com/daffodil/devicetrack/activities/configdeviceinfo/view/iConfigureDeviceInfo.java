package com.daffodil.devicetrack.activities.configdeviceinfo.view;

import com.daffodil.devicetrack.activities.configdeviceinfo.model.DeviceCredentialsModel;

/**
 * <p>
 *     This interface is used to declare callbacks from {@link com.daffodil.devicetrack.activities.configdeviceinfo.presenter.ConfigDevicePresenter}
 *     to {@link ConfigDeviceInfo} Activity.
 * </p>
 */
public interface iConfigureDeviceInfo {

    /**
     * <p>
     *     This method is called when registering device for the first time returns failure.
     *     It resets the Activity view as view for first time
     * </p>
     */
    void registerationFailed();

    /**
     * <p>
     *     This method is called when registerating device api returns success.
     *     It simply displays the success message and stores device ID and device name in shared preferences
     * </p>
     * @param model it is object of {@link DeviceCredentialsModel} model class
     */
    void registerationSuccessFul(DeviceCredentialsModel model);

    /**
     * <p>
     *     This method is called when updating device api returns success.
     *     it only updates the device name as device id is going to be same duh..
     * </p>
     * @param model it is object of {@link DeviceCredentialsModel} model class
     */
    void updateSuccessFull(DeviceCredentialsModel model);

    /**
     * <p>
     *     This method is called when validation succeeds.
     *     The activity then either send register device api or update device api.
     * </p>
     */
    void onValid();

    /**
     * <p>
     *     This method is called when empty field is found.
     *     It then simply displays error dialog
     * </p>
     */
    void onInvalid();
}
