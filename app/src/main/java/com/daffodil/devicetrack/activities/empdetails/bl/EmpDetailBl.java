package com.daffodil.devicetrack.activities.empdetails.bl;

import android.content.Context;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.empdetails.presenter.iEmpDetailPresenter;
import com.daffodil.devicetrack.helpers.AppConst;
import com.daffodil.devicetrack.helpers.DataHandler;
import com.daffodil.devicetrack.helpers.JSONParser;
import com.daffodil.devicetrack.helpers.ServiceCallManager;

import java.util.Map;

public class EmpDetailBl implements iEmpDetailBL {
    private iEmpDetailPresenter presenter;
    private Context context;
    public EmpDetailBl(iEmpDetailPresenter presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    public void tryFreeDevice(String systemMacAddress) {
        String url = AppConst.BASE_URL + "/devices/" + systemMacAddress;
        Map<String, String> simpleHeaders = DataHandler.getSimpleHeaders();
        ServiceCallManager.sendFreeDeviceWithVolley(url, simpleHeaders, context, this);
    }

    @Override
    public void onResponce(String data) {
        presenter.onResponce(JSONParser.getFreeResponceToModel(data));
    }

    @Override
    public void onError(String s) {
        presenter.onError(context.getString(R.string.unknown_error_message));
    }
}
