package com.daffodil.devicetrack.activities.empdetails.view;

import com.daffodil.devicetrack.activities.empdetails.model.FreeDeviceModel;

public interface iEmpDetail {
    void onFreeDeviceSuccessful(FreeDeviceModel model);
    void onError();
}
