package com.daffodil.devicetrack.activities.homescreen.view;

/**
 * <p>
 *     This interface is created for interaction between {@link MainActivity} and its presenter.
 *     Basically the presenter calls methods that are declared in {@link iMainActivity} interface.
 *     This interface is implemented by {@link MainActivity}.
 * </p>
 */
public interface iMainActivity {

    /**
     * <p>
     *     this method is called when a user have already taken the device.
     *     after this when we open {@link MainActivity} the intent is transferred to {@link com.daffodil.devicetrack.activities.empdeviceform.view.EmpDeviceForm}
     * </p>
     */
    public void onDeviceAlreadyTaken();
}
