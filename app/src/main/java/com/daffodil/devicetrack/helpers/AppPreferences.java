package com.daffodil.devicetrack.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.daffodil.devicetrack.R;
import com.daffodil.devicetrack.activities.empdeviceform.model.EmpDeviceData;

/**
 * <p>
 * This class is used to interact with {@link SharedPreferences} in Application.
 * It has static methods to get or set values from or to {@link SharedPreferences} respectively
 * </p>
 * <p>
 * Author : Vishu Bhardwaj<br />
 * Date : 21-09-2016
 * </p>
 */
public class AppPreferences {

    /**
     * <p>
     * This method is used to insert or update value corresponding to a location of Asset ID in {@link SharedPreferences}.
     * There can only be one value stored in this location. It is available throughout application and can be retreived
     * whenever required. The value is corresponding to the ID that is mapping one and only one Mobile Device.
     * It can either be a IPOD, TAB or MOBile.
     * </p>
     * <p>
     * The values for assetId are passed in this format, for a IPOD it can be like "DAFFOIPOD-04". For a TAB it can be
     * like "DAFFOTAB-06" and for a mobile its like "DAFFOMOB-09".
     * </p>
     *
     * @param assetId the string asset ID of the mobile device the app is currently running in.
     * @param context the application context
     */
    public static void setAssetId(String assetId, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.asset_id_key), assetId);
        editor.commit();
    }

    /**
     * <p>
     * It is used to get a unique {@link String} ID of the mobile device if saved in. If no ID is saved in it return null.
     * </p>
     * <p>
     * Format of {@link String} ID<br />
     * For ipod its like DAFFOIPOD-03, for TAB its like DAFFOTAB-05 and for a mobile its like DAFFOMOB-09
     * </p>
     *
     * @param context the application context
     * @return mobile device ID if saved previously otherwise null
     */
    public static String getAssetId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        return sharedPref.getString(context.getString(R.string.asset_id_key), null);
    }

    public static void setDeviceId(String deviceId, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.device_id_key), deviceId);
        editor.commit();
    }

    public static String getDeviceId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        return sharedPref.getString(context.getString(R.string.device_id_key), null);
    }

    /**
     * <p>
     * This method is used to clear all the {@link SharedPreferences} that are stored previously.
     * Calling this method is similar to pressing Clear App Data button in app setting.<br />
     * Note : Calling this method unexpectedly disprove the working of the app.
     * Instead call the method {@link AppPreferences#clearAssetId(Context) clearAssetId()}
     * </p>
     *
     * @param context the application context
     */
    public static void clearSharedPreferences(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * <p>
     * This method is used to clear only the Asset ID stored of the mobile device the app is installed in.
     * Calling this method will remove previously stored ID. It will be like running the app when asset id is not saved,
     * </p>
     *
     * @param context the application context
     */
    public static void clearAssetId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(context.getString(R.string.asset_id_key));
        editor.commit();
    }

    public static void setTakeDeviceData(EmpDeviceData data, Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.emp_code_key),data.empCode);
        editor.putString(context.getString(R.string.emp_name_key),data.empName);
        editor.putString(context.getString(R.string.man_name_key),data.managerName);
        editor.putString(context.getString(R.string.pro_name_key),data.project);
        editor.commit();
    }

    public static EmpDeviceData getTakeDeviceData(Context context) {
        EmpDeviceData data = new EmpDeviceData();
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        data.empCode = sharedPref.getString(context.getString(R.string.emp_code_key), null);
        data.empName = sharedPref.getString(context.getString(R.string.emp_name_key), null);
        data.managerName = sharedPref.getString(context.getString(R.string.man_name_key), null);
        data.project = sharedPref.getString(context.getString(R.string.pro_name_key), null);
        return data;
    }

    public static void clearTakeDeviceData(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(context.getString(R.string.emp_code_key));
        editor.remove(context.getString(R.string.emp_name_key));
        editor.remove(context.getString(R.string.man_name_key));
        editor.remove(context.getString(R.string.pro_name_key));
        editor.commit();
    }

    public static void setToken(String token, Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.token_id_key), token);
        editor.commit();
    }

    public static String getToken(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        return sharedPref.getString(context.getString(R.string.token_id_key), null);
    }

    public static void clearToken(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(context.getString(R.string.token_id_key));
        editor.commit();
    }



}
