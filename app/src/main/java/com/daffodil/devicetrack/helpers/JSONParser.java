package com.daffodil.devicetrack.helpers;

import com.daffodil.devicetrack.activities.adminloginscreen.model.AdminCredentialModel;
import com.daffodil.devicetrack.activities.empdetails.model.FreeDeviceModel;
import com.daffodil.devicetrack.activities.viewdevices.model.Owner;
import com.daffodil.devicetrack.activities.viewdevices.model.ViewDeviceDetail;
import com.daffodil.devicetrack.activities.viewdevices.model.ViewDevicesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * <p>
 *     This class is a helper created for parsing string json responce from api calls
 *     to Model objects and return Models.
 * </p>
 */
public class JSONParser {

    /**
     * <p>
     *     this method is used to converts login responce from json string to {@link AdminCredentialModel} model object/
     * </p>
     * @param data string data passed as responce from api calls
     * @return {@link AdminCredentialModel} model class object holding data
     */
    public static AdminCredentialModel getLoginResponceToModel(String data) {
        JSONObject responce = null, auth = null, admin = null;
        try {
            responce = new JSONObject(data);
            auth = responce.optJSONObject(AppConst.AUTH_KEY);
            admin = responce.optJSONObject(AppConst.ADMIN_KEY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AdminCredentialModel model = new AdminCredentialModel();
        try {
            model.setUserId(auth.getString(AppConst.ADMIN_LOGIN_KEY_1));
            model.setUserType(auth.getString(AppConst.ADMIN_LOGIN_KEY_2));
            model.setToken(auth.getString(AppConst.ADMIN_LOGIN_KEY_3));
            model.set_id(auth.getString(AppConst.ADMIN_LOGIN_KEY_4));
            model.setEmail(admin.getString(AppConst.ADMIN_LOGIN_KEY_5));
            model.setFullName(admin.getString(AppConst.ADMIN_LOGIN_KEY_6));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return model;
    }

    public static ViewDevicesModel getViewResponceToModel(String data) {
        JSONObject responce = null, item=null, owner=null;
        JSONArray itemsList=null;
        try {
            responce = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ViewDevicesModel model = new ViewDevicesModel();
        if (responce != null) {
            model.skip = responce.optInt(AppConst.VIEW_DEVICE_KEY_1);
            model.limit = responce.optInt(AppConst.VIEW_DEVICE_KEY_2);
            model.totalCount = responce.optInt(AppConst.VIEW_DEVICE_KEY_14);
            model.hasPrev = responce.optBoolean(AppConst.VIEW_DEVICE_KEY_15);
            model.hasNext = responce.optBoolean(AppConst.VIEW_DEVICE_KEY_16);
            model.itemCount = responce.optInt(AppConst.VIEW_DEVICE_KEY_17);

            if(model.itemCount!=0){
                model.deviceDetails = new ArrayList<>();
                itemsList = responce.optJSONArray(AppConst.VIEW_DEVICE_KEY_3);
                for(int i=0;i<model.itemCount;i++){
                    ViewDeviceDetail details = new ViewDeviceDetail();
                    try {
                        item = (JSONObject) itemsList.get(i);
                        details.macAddress = item.optString(AppConst.VIEW_DEVICE_KEY_4);
                        details.name = item.optString(AppConst.VIEW_DEVICE_KEY_5);
                        details.model = item.optString(AppConst.VIEW_DEVICE_KEY_6);
                        details._id = item.optString(AppConst.VIEW_DEVICE_KEY_7);
                        owner = item.optJSONObject(AppConst.VIEW_DEVICE_KEY_8);
                        details.owner = null;
                        if(owner!=null) {
                            details.owner = new Owner();
                            details.owner.empCode = owner.optString(AppConst.VIEW_DEVICE_KEY_9);
                            details.owner.name = owner.optString(AppConst.VIEW_DEVICE_KEY_10);
                            details.owner.manager = owner.optString(AppConst.VIEW_DEVICE_KEY_11);
                            details.owner.project = owner.optString(AppConst.VIEW_DEVICE_KEY_12);
                            details.owner._id = owner.optString(AppConst.VIEW_DEVICE_KEY_13);
                        }
                        model.deviceDetails.add(details);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return model;
    }

    public static FreeDeviceModel getFreeResponceToModel(String data) {
        JSONObject responce = null;
        try {
            responce = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        FreeDeviceModel model = new FreeDeviceModel();
        model._id = responce.optString(AppConst.VIEW_DEVICE_KEY_7);
        model.name = responce.optString(AppConst.VIEW_DEVICE_KEY_5);
        model.model = responce.optString(AppConst.VIEW_DEVICE_KEY_6);
        model.macAddress = responce.optString(AppConst.VIEW_DEVICE_KEY_4);
        return model;
    }
}
