package com.daffodil.devicetrack.helpers;

import java.util.HashMap;
import java.util.Map;

public class DataHandler {
    public static Map<String, String> getSimpleHeaders() {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(AppConst.LOGIN_HEADER_1, AppConst.ANDROID_API_KEY);
        headerMap.put(AppConst.LOGIN_HEADER_2, AppConst.LOGIN_HEADER_2_VALUE);
        return headerMap;
    }

    public static Map<String,String> getCompleHandler(String token) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(AppConst.LOGIN_HEADER_1, AppConst.ANDROID_API_KEY);
        headerMap.put(AppConst.LOGIN_HEADER_2, AppConst.LOGIN_HEADER_2_VALUE);
        headerMap.put(AppConst.REGISTER_HEADER_3, token);
        return headerMap;
    }
}
