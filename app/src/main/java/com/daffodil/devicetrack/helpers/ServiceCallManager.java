package com.daffodil.devicetrack.helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daffodil.devicetrack.activities.adminloginscreen.bl.iAdminLoginBl;
import com.daffodil.devicetrack.activities.configdeviceinfo.bl.iConfigDevBL;
import com.daffodil.devicetrack.activities.empdetails.bl.iEmpDetailBL;
import com.daffodil.devicetrack.activities.empdeviceform.bl.EmpDevBL;
import com.daffodil.devicetrack.activities.viewdevices.bl.iViewDeviceBL;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceCallManager {

    public static void sendLoginRequestWithVolly(final String username, final String password, Context context, final iAdminLoginBl listener) {
        RequestQueue queue = Volley.newRequestQueue(context);

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", username);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = AppConst.BASE_URL + "/login/admin";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errror",error.networkResponse.statusCode+""+new String(error.networkResponse.data));
                listener.onError(new String(error.networkResponse.data));
            }
        })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put(AppConst.LOGIN_HEADER_1, AppConst.ANDROID_API_KEY);
                headerMap.put(AppConst.LOGIN_HEADER_2, AppConst.LOGIN_HEADER_2_VALUE);
                return headerMap;
            }

        };

        queue.add(request);


    }

    public static void sendRegisterDeviceRequestWithVolley(String macAddress, String deviceName, String modelNumber, final String token, final iConfigDevBL listener, Context context) {
        RequestQueue queue = Volley.newRequestQueue(context);

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConst.REGISTER_BODY_1, macAddress);
            jsonObject.put(AppConst.REGISTER_BODY_2, deviceName);
            jsonObject.put(AppConst.REGISTER_BODY_3, modelNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = AppConst.BASE_URL+"/devices";
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(AppConst.LOG_CONST, response);
                listener.onResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errror",error.networkResponse.statusCode+""+new String(error.networkResponse.data));
                listener.onError(new String(error.networkResponse.data));
            }
        })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put(AppConst.LOGIN_HEADER_1, AppConst.ANDROID_API_KEY);
                headerMap.put(AppConst.LOGIN_HEADER_2, AppConst.LOGIN_HEADER_2_VALUE);
                headerMap.put(AppConst.REGISTER_HEADER_3, token);
                return headerMap;
            }

        };

        queue.add(request);
    }

    public static void sendLoginRequest(final String username, final String password, Context context, final iAdminLoginBl listener) {

final String bodyData = "{'"+AppConst.LOGIN_BODY_1+"':'"+username+"','"+AppConst.LOGIN_BODY_2+"':'"+password+"'}";
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject(bodyData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(
                Request.Method.POST,
                "http://172.18.4.182:8080/login/admin",
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error : ",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("email",username);
                params.put("password",password);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put(AppConst.LOGIN_HEADER_1, AppConst.ANDROID_API_KEY);
                headerMap.put(AppConst.LOGIN_HEADER_2, AppConst.LOGIN_HEADER_2_VALUE);
                return headerMap;
            }
/*
            @Override
            public byte[] getBody() throws AuthFailureError {

                return bodyData.getBytes();
            }*/
        };
        queue.add(sr);

        /*StringRequest st1 = new StringRequest(Request.Method.POST,"http://jsonplaceholder.typicode.com/posts", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error : ",error.toString());
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("title","foo");
                params.put("body","bar");
                params.put("userId",String.valueOf(1));
                return params;
            }
*//*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put(AppConst.LOGIN_HEADER_1, AppConst.ANDROID_API_KEY);
                headerMap.put(AppConst.LOGIN_HEADER_2, AppConst.LOGIN_HEADER_2_VALUE);
                return headerMap;
            }*//*
        };
        queue.add(st1);*/


        //final JSONObject[] loginObject = {null};
        String url = AppConst.BASE_URL+"/login/admin";
        //RequestQueue queue = Volley.newRequestQueue(context);
        //String bodyData = "{'"+AppConst.LOGIN_BODY_1+"':'"+username+"','"+AppConst.LOGIN_BODY_2+"':'"+password+"'}";
        ///Map<String, String> bodyMap = new HashMap<>();
        //bodyMap.put(AppConst.LOGIN_BODY_1, username);
        //bodyMap.put(AppConst.LOGIN_BODY_2, password);
        //JSONObject jsonBody = null;
        //jsonBody = new JSONObject(bodyMap);
//
//        final JSONObject[] responce1 = {null};
//        JsonObjectRequest loginRequest = new JsonObjectRequest(
//                Request.Method.POST,
//                url,
//                jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        responce1[0] = response;
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.d(AppConst.LOG_CONST, AppConst.LOGIN_REQUEST_FAILED_LOG);
//                    }
//                }
//        ){
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> headerMap = new HashMap<>();
//                headerMap.put(AppConst.LOGIN_HEADER_1, AppConst.ANDROID_API_KEY);
//                headerMap.put(AppConst.LOGIN_HEADER_2, AppConst.LOGIN_HEADER_2_VALUE);
//                return headerMap;
//            }
//        };
//        queue.add(loginRequest);
    }

    public static void sendUpdateDeviceRequestWithVilley(String macAddress, String deviceName, String modelNumber, final String token, final iConfigDevBL listener, Context context, String deviceId) {
        RequestQueue queue = Volley.newRequestQueue(context);

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(AppConst.REGISTER_BODY_1, macAddress);
            jsonObject.put(AppConst.REGISTER_BODY_2, deviceName);
            jsonObject.put(AppConst.REGISTER_BODY_3, modelNumber);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = AppConst.BASE_URL+"/device/"+deviceId;
        StringRequest request = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(AppConst.LOG_CONST, "Update device "+response);
                listener.onUpdateResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errror",error.networkResponse.statusCode+""+new String(error.networkResponse.data));
                listener.onError(new String(error.networkResponse.data));
            }
        })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put(AppConst.LOGIN_HEADER_1, AppConst.ANDROID_API_KEY);
                headerMap.put(AppConst.LOGIN_HEADER_2, AppConst.LOGIN_HEADER_2_VALUE);
                headerMap.put(AppConst.REGISTER_HEADER_3, token);
                return headerMap;
            }

        };

        queue.add(request);
    }

    public static void sendViewDeviceRequestWithVolley(String url, final Map<String, String> header, Context context, final iViewDeviceBL listener) {
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errror",error.networkResponse.statusCode+""+new String(error.networkResponse.data));
                listener.onOtherError(new String(error.networkResponse.data));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }

        };

        queue.add(request);
    }

    public static void sendDeleteDeviceRequestWithVolley(String url, final Map<String, String> header, Context context, final iViewDeviceBL listener, final int position) {
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onDeleteResponce(response, position);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errror",error.networkResponse.statusCode+""+new String(error.networkResponse.data));
                listener.onDeleteError(new String(error.networkResponse.data));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }

        };

        queue.add(request);
    }

    public static void sendBookRequestWithVolley(String url, final Map<String, String> header, final JSONObject jsonObject, Context context, final EmpDevBL listener) {
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errror",error.networkResponse.statusCode+""+new String(error.networkResponse.data));
                listener.onError(new String(error.networkResponse.data));
            }
        })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return jsonObject.toString().getBytes();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }

        };

        queue.add(request);
    }

    public static void sendFreeDeviceWithVolley(String url, final Map<String, String> header, Context context, final iEmpDetailBL listener){
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest request = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onResponce(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errror",error.networkResponse.statusCode+""+new String(error.networkResponse.data));
                listener.onError(new String(error.networkResponse.data));
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return header;
            }

        };
        queue.add(request);
    }
}
