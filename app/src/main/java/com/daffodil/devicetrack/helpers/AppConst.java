package com.daffodil.devicetrack.helpers;

public class AppConst {
    public static int VIEW_TYPE_FREE = 0;
    public static int VIEW_TYPE_IN_USE = 1;
    public static int VIEW_TYPE_LOAD_MORE = 2;

    public static String ANDROID_API_KEY = "a8de306db2f93b192ca7127657eddfc014e37313aa6259a5f653c3bfb57aaaf8";
    // public static String BASE_URL = "http://172.18.4.182:8080";
    public static String BASE_URL = "http://52.5.234.73";

    public static String LOG_CONST = "deviceTrackerLog";


    // log message
    public static String LOGIN_REQUEST_FAILED_LOG = "Login request failed.";


    // keys
    public static String LOGIN_HEADER_1 = "api_key";
    public static String LOGIN_HEADER_2 = "Content-Type";
    public static String LOGIN_HEADER_2_VALUE = "application/json";
    public static String LOGIN_BODY_1 = "email";
    public static String LOGIN_BODY_2 = "password";

    public static String INVALID_LOGIN_MESSAGE = "Format validation failed (Should be an email address.)";
    public static String INVALID_CREDENTIALS = "Login credentials do not match any registered user.";
    public static String INVALID_DEVICE = "Device not found with requested Id. Please try with different id.";


    public static String ADMIN_LOGIN_KEY_1 = "userId";
    public static String ADMIN_LOGIN_KEY_2 = "userType";
    public static String ADMIN_LOGIN_KEY_3 = "token";
    public static String ADMIN_LOGIN_KEY_4 = "_id";
    public static String ADMIN_LOGIN_KEY_5 = "email";
    public static String ADMIN_LOGIN_KEY_6 = "fullName";

    public static String AUTH_KEY = "auth";
    public static String ADMIN_KEY = "admin";

    public static String REGISTER_BODY_1 = "macAddress";
    public static String REGISTER_BODY_2 = "name";
    public static String REGISTER_BODY_3 = "model";
    public static String REGISTER_HEADER_3 = "auth_token";

    public static String REGISTER_ERROR_KEY_1 = "DEVICE_ALREADY_REGISTERED";
    public static String DEVICE_ALREADY_REGISTERED = "Requested Device is already registered. Please register another device.";
    public static String INVALID_UPDATE_ID = "Format validation failed (Should be an object id.).";

    public static String VIEW_DEVICE_KEY_1 = "skip";
    public static String VIEW_DEVICE_KEY_2 = "limit";
    public static String VIEW_DEVICE_KEY_3 = "items";
    public static String VIEW_DEVICE_KEY_4 = "macAddress";
    public static String VIEW_DEVICE_KEY_5 = "name";
    public static String VIEW_DEVICE_KEY_6 = "model";
    public static String VIEW_DEVICE_KEY_7 = "_id";
    public static String VIEW_DEVICE_KEY_8 = "owner";
    public static String VIEW_DEVICE_KEY_9 = "empCode";
    public static String VIEW_DEVICE_KEY_10 = "name";
    public static String VIEW_DEVICE_KEY_11 = "manager";
    public static String VIEW_DEVICE_KEY_12 = "project";
    public static String VIEW_DEVICE_KEY_13 = "_id";
    public static String VIEW_DEVICE_KEY_14 = "total_count";
    public static String VIEW_DEVICE_KEY_15 = "hasPrev";
    public static String VIEW_DEVICE_KEY_16 = "hasNext";
    public static String VIEW_DEVICE_KEY_17 = "item_count";

}
