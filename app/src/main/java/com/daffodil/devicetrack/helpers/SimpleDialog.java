package com.daffodil.devicetrack.helpers;

import android.content.Context;
import android.support.v7.app.AlertDialog;

/**
 * <p>
 * This class provides static methods to create dialog.
 * The methods take title and message and returns AlertDialog.<br />
 * Use {@link #getSimpleDialog(String, String, Context) geteSimpleDialog()} method to show dialog.<br />
 * Usage<br />
 * SimpleDialog.getSimpleDialog("Title", "message", this).show();<br />
 * This is just a helper class to provide AlertDialog. You need to call .show() method to run<br />
 * <br />
 * Author : Vishu Bhardwaj<br />
 * Date : 21-09-2016
 * </p>
 */
public class SimpleDialog {

    /**
     * <p>
     * This method creates a {@link AlertDialog} with provided Title and Message in presence of the aplication context.
     * The method create {@link AlertDialog} object using static {@link android.support.v7.app.AlertDialog.Builder} class's static method {@link AlertDialog.Builder#create() create()}.
     * After that the method sets title and message as provided and sets simple notice icon.
     * </p>
     *
     * @param title   The title to be displayed as {@link String}
     * @param message The message to be shown {@link String}
     * @param context The application context as this object from activity
     * @return {@link AlertDialog} object. Note you need to call {@link AlertDialog#show() show()} method to display this dialog.
     */
    public static AlertDialog getSimpleDialog(String title, String message, Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);

        // Setting OK Button
        // alertDialog.setButton("OK", ... listener ... );

        // Showing Alert Message
        // alertDialog,show();

        //returns the alert dialog
        return alertDialog;
    }


}
